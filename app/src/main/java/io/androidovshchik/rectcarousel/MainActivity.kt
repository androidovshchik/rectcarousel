package io.androidovshchik.rectcarousel

import android.os.Bundle
import com.github.androidovshchik.BaseV7Activity

class MainActivity : BaseV7Activity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }
}
