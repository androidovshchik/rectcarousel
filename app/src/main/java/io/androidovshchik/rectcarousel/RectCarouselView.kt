package io.androidovshchik.rectcarousel

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.SurfaceHolder
import android.view.SurfaceView
import timber.log.Timber

class RectCarouselView : SurfaceView {

    private lateinit var thread: RectCarouselThread

    // square side size from min part of device
    private var square = 0

    private var startX = 0f
    private var startY = 0f

    private val partOfWidth = 0.333f
    private val partOfHeight = 0.333f

    private var activeAngel = 0f
    private var persistentAngel = 0f

    private val paint = Paint(Paint.ANTI_ALIAS_FLAG)

    private val rect1 = RectF()
    private val rect2 = RectF()
    private val rect3 = RectF()
    private val rect4 = RectF()
    private val rect5 = RectF()
    private val rect6 = RectF()
    private val rect7 = RectF()
    private val rect8 = RectF()
    private val rectC = RectF()
    private val bounds = Rect()

    private val surfaceCallback = object : SurfaceHolder.Callback {

        override fun surfaceChanged(holder: SurfaceHolder, format: Int, width: Int, height: Int) {
            notifyRects()
        }

        override fun surfaceCreated(holder: SurfaceHolder) {
            thread = RectCarouselThread(getHolder(), getInstance())
            thread.isRunning = true
            thread.start()
        }

        override fun surfaceDestroyed(holder: SurfaceHolder) {
            var retry = true
            thread.isRunning = false
            while (retry) {
                try {
                    thread.join()
                    retry = false
                } catch (e: Exception) {
                    Timber.e(e)
                }
            }
        }
    }

    constructor(context: Context) : super(context)

    constructor(context: Context, attrs: AttributeSet) : super(context, attrs)

    constructor(context: Context, attrs: AttributeSet, defStyle: Int) : super(context, attrs, defStyle)

    override fun onFinishInflate() {
        super.onFinishInflate()
        isFocusable = true
        holder.addCallback(surfaceCallback)
        paint.textSize = 36f * resources.displayMetrics.scaledDensity
    }

    /*
    *  6 7 8
    *  5 C 1
    *  4 3 2
    * */
    public override fun onDraw(canvas: Canvas) {
        // clear canvas with white color
        canvas.drawARGB(255, 255, 255, 255)
        val textOffsetX = square * partOfWidth / 2
        val textOffsetY = square * partOfHeight / 2
        var text = "1"
        paint.style = Paint.Style.FILL

        paint.color = Color.RED
        canvas.drawRect(rect1, paint)
        paint.color = Color.BLACK
        paint.getTextBounds(text, 0, text.length, bounds)
        canvas.drawText(text, rect1.left + textOffsetX - bounds.width() / 2,
            rect1.top + textOffsetY + bounds.height() / 2, paint)

        paint.color = Color.MAGENTA
        canvas.drawRect(rect2, paint)
        paint.color = Color.BLACK
        text = "2"
        paint.getTextBounds(text, 0, text.length, bounds)
        canvas.drawText(text, rect2.left + textOffsetX - bounds.width() / 2,
            rect2.top + textOffsetY + bounds.height() / 2, paint)

        paint.color = Color.RED
        canvas.drawRect(rect3, paint)
        paint.color = Color.BLACK
        text = "3"
        paint.getTextBounds(text, 0, text.length, bounds)
        canvas.drawText(text, rect3.left + textOffsetX - bounds.width() / 2,
            rect3.top + textOffsetY + bounds.height() / 2, paint)

        paint.color = Color.MAGENTA
        canvas.drawRect(rect4, paint)
        paint.color = Color.BLACK
        text = "4"
        paint.getTextBounds(text, 0, text.length, bounds)
        canvas.drawText(text, rect4.left + textOffsetX - bounds.width() / 2,
            rect4.top + textOffsetY + bounds.height() / 2, paint)

        paint.color = Color.RED
        canvas.drawRect(rect5, paint)
        paint.color = Color.BLACK
        text = "5"
        paint.getTextBounds(text, 0, text.length, bounds)
        canvas.drawText(text, rect5.left + textOffsetX - bounds.width() / 2,
            rect5.top + textOffsetY + bounds.height() / 2, paint)

        paint.color = Color.MAGENTA
        canvas.drawRect(rect6, paint)
        paint.color = Color.BLACK
        text = "6"
        paint.getTextBounds(text, 0, text.length, bounds)
        canvas.drawText(text, rect6.left + textOffsetX - bounds.width() / 2,
            rect6.top + textOffsetY + bounds.height() / 2, paint)

        paint.color = Color.RED
        canvas.drawRect(rect7, paint)
        paint.color = Color.BLACK
        text = "7"
        paint.getTextBounds(text, 0, text.length, bounds)
        canvas.drawText(text, rect7.left + textOffsetX - bounds.width() / 2,
            rect7.top + textOffsetY + bounds.height() / 2, paint)

        paint.color = Color.MAGENTA
        canvas.drawRect(rect8, paint)
        paint.color = Color.BLACK
        text = "8"
        paint.getTextBounds(text, 0, text.length, bounds)
        canvas.drawText(text, rect8.left + textOffsetX - bounds.width() / 2,
            rect8.top + textOffsetY + bounds.height() / 2, paint)

        paint.color = Color.GREEN
        canvas.drawRect(rectC, paint)
    }

    /*
    *  6 7 8
    *  5 C 1
    *  4 3 2
    * */
    private fun notifyRects() {
        square = Math.min(width, height)
        val w = square * partOfWidth
        val h = square * partOfHeight
        Timber.d("index ?. width $width")
        Timber.d("index ?. height $height")
        Timber.d("index ?. square $square")
        Timber.d("index ?. w $w")
        Timber.d("index ?. h $h")
        Timber.d("index ?. w / 2 ${w / 2}")
        Timber.d("index ?. h / 2 ${h / 2}")
        Timber.d("index ?. x1 ${width / 2} y1 ${height / 2}")
        rect1.set(getRectPosition(0))
        rect2.set(getRectPosition(1))
        rect3.set(getRectPosition(2))
        rect4.set(getRectPosition(3))
        rect5.set(getRectPosition(4))
        rect6.set(getRectPosition(5))
        rect7.set(getRectPosition(6))
        rect8.set(getRectPosition(7))
        rectC.set(getOffsetX() + (square - w) / 2, getOffsetY() + (square - h) / 2,
            getOffsetX() + (square + w) / 2, getOffsetY() + (square + h) / 2)
    }

    private fun getRectPosition(index: Int): RectF {
        var rectAngle = persistentAngel + activeAngel
        for (i in 0 until index) {
            rectAngle += 45f
        }
        Timber.d("index $index. rectAngle $rectAngle")
        val w = square * partOfWidth
        val h = square * partOfHeight
        val r = square / 2
        val x2 = width / 2 + r * Math.cos(Math.toRadians(rectAngle.toDouble())).toFloat()
        val y2 = height / 2 + r * Math.sin(Math.toRadians(rectAngle.toDouble())).toFloat()
        Timber.d("index $index. x2 $x2 y2 $y2")
        var centerX: Float
        var centerY: Float
        // x = (y - b) / k
        // y = kx + b
        val k = if (width / 2f != x2) {
            (height / 2 - y2) / (width / 2 - x2)
        } else {
            Float.MAX_VALUE
        }
        val b = y2 - k * x2
        Timber.d("index $index. y = ${k}x + $b")
        if (x2 > width / 2) {
            if (y2 > height / 2) {
                Timber.d("index $index. 0 < angle < 90")
                centerX = (height - getOffsetY() - h / 2 - b) / k
                centerY = k * (width - getOffsetX() - w / 2) + b
                Timber.d("index $index. centerX $centerX centerY $centerY")
                if (!isCenterXPreferred(centerX, centerY, width - getOffsetX(), height - getOffsetY())) {
                    centerX = (centerY - b) / k
                } else {
                    centerY = k * centerX + b
                }
            } else if (y2 < height / 2) {
                Timber.d("index $index. 270 < angle < 360")
                centerX = (getOffsetY() + h / 2 - b) / k
                centerY = k * (width - getOffsetX() - w / 2) + b
                if (!isCenterXPreferred(centerX, centerY, width - getOffsetX(), getOffsetY())) {
                    centerX = (centerY - b) / k
                } else {
                    centerY = k * centerX + b
                }
            } else {
                Timber.d("index $index. angle == 0")
                centerX = width - w / 2
                centerY = height / 2f
            }
        } else if (x2 < width / 2) {
            if (y2 > height / 2) {
                Timber.d("index $index. 90 < angle < 180")
                centerX = (height - getOffsetY() - h / 2 - b) / k
                centerY = k * (getOffsetX() + w / 2) + b
                if (!isCenterXPreferred(centerX, centerY, getOffsetX(), height - getOffsetY())) {
                    centerX = (centerY - b) / k
                } else {
                    centerY = k * centerX + b
                }
            } else if (y2 < height / 2) {
                Timber.d("index $index. 180 < angle < 270")
                centerX = (getOffsetY() + h / 2 - b) / k
                centerY = k * (getOffsetX() + w / 2) + b
                if (!isCenterXPreferred(centerX, centerY, getOffsetX(), getOffsetY())) {
                    centerX = (centerY - b) / k
                } else {
                    centerY = k * centerX + b
                }
            } else {
                Timber.d("index $index. angle == 180")
                centerX = w / 2
                centerY = height / 2f
            }
        } else if (y2 > height / 2) {
            Timber.d("index $index. angle == 90")
            centerX = getOffsetX() + width / 2f
            centerY = height - getOffsetY() - h / 2
        } else {
            Timber.d("index $index. angle == 270")
            centerX = getOffsetX() + width / 2f
            centerY = getOffsetY() + h / 2
        }
        Timber.d("index $index. centerX $centerX centerY $centerY")
        return RectF(centerX - w / 2, centerY - h / 2, centerX + w / 2, centerY + h / 2)
    }

    private fun isCenterXPreferred(centerX: Float, centerY: Float, cornerX: Int, cornerY: Int): Boolean {
        val xValid = cornerX < width / 2 && centerX > cornerX || cornerX > width / 2 && centerX < cornerX
        val yValid = cornerY < height / 2 && centerY > cornerY || cornerY > height / 2 && centerY < cornerY
        val xDiff = if (cornerX < width / 2) centerX - getOffsetX() else cornerX - centerX
        val yDiff = if (cornerY < height / 2) centerY - getOffsetY() else cornerY - centerY
        return xValid && xDiff > yDiff || !yValid
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent): Boolean {
        when (event.action) {
            MotionEvent.ACTION_DOWN -> {
                Timber.d("ACTION_DOWN x ${event.x} y ${event.y}")
                startX = event.x
                startY = event.y
            }
            MotionEvent.ACTION_MOVE -> {
                Timber.d("ACTION_MOVE x ${event.x} y ${event.y}")
                var endX = event.x - width / 2
                var endY = height / 2 - event.y
                if (endX != 0f && endY != 0f) {
                    val angleB = coordinate2Angle(endX, endY)
                    endX = startX - width / 2
                    endY = height / 2 - startY
                    val angleA = coordinate2Angle(endX, endY)
                    activeAngel = angleA - angleB
                    Timber.d("activeAngel $activeAngel")
                    notifyRects()
                }
            }
            MotionEvent.ACTION_UP, MotionEvent.ACTION_CANCEL -> {
                persistentAngel += activeAngel
                while (persistentAngel > 360) {
                    persistentAngel -= 360f
                }
                while (persistentAngel < 0) {
                    persistentAngel += 360f
                }
                Timber.d("persistentAngel $persistentAngel")
                activeAngel = 0f
            }
        }
        return true
    }

    private fun coordinate2Angle(x: Float, y: Float): Float {
        var result = Math.toDegrees(Math.atan2(y.toDouble(), x.toDouble()))
        if (result < 0) {
            result += 360
        }
        return result.toFloat()
    }

    private fun getOffsetX(): Int {
        return if (width < height) {
            0
        } else {
            (width - height) / 2
        }
    }

    private fun getOffsetY(): Int {
        return if (width < height) {
            (height - width) / 2
        } else {
            0
        }
    }

    public override fun onDetachedFromWindow() {
        super.onDetachedFromWindow()
        holder.removeCallback(surfaceCallback)
    }

    override fun hasOverlappingRendering(): Boolean {
        return false
    }

    private fun getInstance(): RectCarouselView {
        return this
    }
}