package io.androidovshchik.rectcarousel

import android.annotation.SuppressLint
import android.graphics.Canvas
import android.view.SurfaceHolder

class RectCarouselThread(private val threadSurfaceHolder: SurfaceHolder,
                         private val rectThreadSurfaceView: RectCarouselView) : Thread() {

    var isRunning = false

    @SuppressLint("WrongCall")
    override fun run() {
        while (isRunning) {
            var canvas: Canvas? = null
            try {
                canvas = threadSurfaceHolder.lockCanvas(null)
                synchronized(threadSurfaceHolder) {
                    if (canvas != null) {
                        rectThreadSurfaceView.onDraw(canvas)
                    }
                }
            } finally {
                if (canvas != null) {
                    threadSurfaceHolder.unlockCanvasAndPost(canvas)
                }
            }
        }
    }
}